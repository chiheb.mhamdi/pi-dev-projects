<?php

namespace TacheBundle\Repository;

use Doctrine\DBAL\DBALException;

/**
 * TacheRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TacheRepository extends \Doctrine\ORM\EntityRepository
{

    public function Recherche($commentaire)
    {
        $query = $this->getEntityManager()->createQuery("Select p FROM TacheBundle\Entity\Tache p WHERE p.commentaire=:commentaire")
                        ->setParameter('commentaire',$commentaire);
                        return $query->getResult();
    }
    public function TriParCommentaire()
    {

        $query = $this->getEntityManager()->createQuery("Select p FROM TacheBundle\Entity\Tache p ORDER BY p.commentaire");
        return $query->getResult();

    }


    public function TacheEmploye($id)
    {
        $query = $this->getEntityManager()->createQuery("SELECT t from TacheBundle\Entity\Tache t WHERE t.idEmploye=:id")
        ->setParameter('id',$id);
        return $query->getResult();
    }

    public function getTacheEmploye($prenom)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT * FROM tache WHERE idEmploye=(SELECT id from employe where prenom='$prenom')";
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {

        }

        $stmt->execute();
        return $stmt->fetchAll();
    }


    public function RechercheEmpTache($prenom)
    {
    $query = $this->getEntityManager()->createQuery("Select p FROM TacheBundle\Entity\Tache p WHERE p.idEmploye=(Select u.id FROM EmployeBundle\Entity\Employe u WHERE u.prenom=:prenom)")
        ->setParameter('prenom',$prenom);
    return $query->getResult();
    }

    public function tacheEffectues()
    {
        $query = $this->getEntityManager()->createQuery("SELECT count(p) AS nbTache FROM TacheBundle:Tache p WHERE p.etat=1 GROUP by p.idEmploye");
        return $query->getResult();
    }


    public function getTachesEffectues($id)
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "select COUNT(*) as nbTaches FROM tache where etat=1 and idEmploye='$id'   ";
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {

        }
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getTachesNonEffectues($id){

        $conn = $this->getEntityManager()->getConnection();
        $sql = "select COUNT(*) as nbTaches FROM tache where etat=0 and idEmploye='$id'   ";
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {

        }

        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getConge($id){

        $conn = $this->getEntityManager()->getConnection();
        $sql = "select COUNT(*) as nbConges FROM conge where etat=1 and idEmploye='$id'   ";
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {
        }
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getCongeEnCours($id){
        $conn = $this->getEntityManager()->getConnection();
        $sql = "select COUNT(*) as nbConges FROM conge where etat=0 and idEmploye='$id'   ";
        try {
            $stmt = $conn->prepare($sql);
        } catch (DBALException $e) {
        }
        $stmt->execute();
        return $stmt->fetchAll();
    }






}

