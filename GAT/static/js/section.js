const sectionForm = document.getElementById('section-form')
$( "form" ).on( "submit", function(e) {
    $("form").toggle();
    $("#section-content").hide();
    e.preventDefault();
    const fd = new FormData(); 
    var nameSection=document.getElementById('nameSection').value
    const csrf=document.getElementsByName('csrfmiddlewaretoken')
    const nameS=document.getElementById('nameS')
    fd.append('name',nameSection)
    console.log(typeof(nameSection))
    fd.append('csrfmiddlewaretoken',csrf[0].value)
    $.ajax({
        type: "POST",
        url: 'add-section',
        data: fd,
        success:function(data)
        {
            $("#display-section").show();
            nameS.innerHTML = nameSection;

        },
        cache:false,
        processData: false,
        contentType: false,
      })
      $("form")[0].reset();
});
