const uploadForm = document.getElementById('upload-form')
const image=document.getElementById('image')
const video = document.getElementById('id-video')
const alertBox=document.getElementById('alert-box')
const videoBox=document.getElementById('video-box')
const progressBox=document.getElementById('progress-box')
const cancelBox=document.getElementById('cancel-box')
const cancelBtn=document.getElementById('cancel-btn')
const csrf=document.getElementsByName('csrfmiddlewaretoken')

video.addEventListener('change',()=>{
    progressBox.classList.remove('not-visible')
    cancelBox.classList.remove('not-visible') 
    var name_c=document.getElementById('name-c').value
    var category=document.getElementById('category').value
    var level=document.getElementById('level').value
    var description=document.getElementById('description').value
    const image_data=image.files[0]
    const video_data=video.files[0] 
    const url=URL.createObjectURL(video_data)
    const fd = new FormData() 
    fd.append('name',name_c)
    fd.append('category',category)
    fd.append('level',level)
    fd.append('description',description)
    fd.append('image',image_data)
    fd.append('video',video_data)
    fd.append('csrfmiddlewaretoken',csrf[0].value)
    $.ajax({
        type:'POST',
        url:courseForm.action,
        enctype:'multipart/form-data',
        data:fd,
        beforeSend:function(){
            alertBox.innerHTML= ""
            videoBox.innerHTML = ""
 
        },
        xhr:function(){
            const xhr=new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress',e=>{
               console.log(name_c)
               if(e.lengthComputable){
                   const percent = e.loaded / e.total * 100 
                   progressBox.innerHTML = `<div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: ${percent}%" aria-valuenow="${percent}" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <p>${percent.toFixed(1)}
                    `
               }
            })
            cancelBtn.addEventListener('click',()=>{
                xhr.abort() 
                setTimeout(()=>{
                    uploadForm.reset()
                    progressBox.innerHTML=""
                    alertBox.innerHTML = ""
                    cancelBox.classList.add('not-visible')
                }, 2000)
            })
            return xhr
        },
        success:function(response){
            videoBox.innerHTML=`<video width="800" controls>
            <source src="${url}" type="video/mp4">
            </video> `

            alertBox.innerHTML=`<div class="alert alert-success" role="alert">
                                Successfully uploaded the video
                                </div>`

                cancelBox.classList.add('not-visible')

        },
        error: function(error){
            console.log(error)

        },
        cache:false,
        contentType:false,
        processData:false,
    })
})