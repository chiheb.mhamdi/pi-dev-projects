from accounts.models import Notification
from django.shortcuts import render
from courses.models import Course
from django.contrib.auth.models import Group
from django.contrib.auth.models import User

# Create your views here.
from accounts.decorators import unauthenticated_user,allowed_users,admin_only


@admin_only
def redirectUser(request) : 
    roles=request.user.groups.all()
    for r in roles : 
        print(r.name)
    courses=Course.objects.all()
    notifications=Notification.objects.order_by('-created_date')[:5]
    print(roles)
    return render(request,'front/index.html',{'courses':courses,'roles':roles,'notifications':notifications})

def index(request):
    roles_user=request.user.groups.all() 
    print(request.user)
    roles=[]
    for r in roles_user : 
        roles.append(r.name)
    print(roles)
    courses=Course.objects.all()
    notifications=Notification.objects.order_by('-created_date')[:5]
    return render(request,'front/index.html',{'courses':courses,'roles':roles,'notifications':notifications})