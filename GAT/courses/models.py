from django.db import models
from django.db.models.base import Model
from django.db.models.fields import DateTimeField
from django.db.models.fields.related import ForeignKey
from django.utils import timezone
from django.contrib.auth.models import User
from PIL import Image
# Create your models here.
class Category (models.Model): 
    name = models.CharField(max_length=100)
    slug = models.CharField(max_length=100)
    num_courses = models.IntegerField(default=0)
    created_date = models.DateTimeField(default=timezone.now)
    def __str__(self):
        return self.name

class Course (models.Model) : 
    name=models.CharField(max_length=100)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    description=models.TextField(max_length=200)
    level_choices=(
        ('Beignner','Beignner'),
        ('Intermediate','Intermediate'),
        ('Advanced' , 'Advanced')
    ) 

    level=models.CharField(max_length=30,blank=True,null=False,choices=level_choices)
    image=models.ImageField(upload_to='pics',default='pics/default.png') 
    video=models.FileField(upload_to='pics')
    created_date = models.DateTimeField(default=timezone.now)
    is_approved=models.BooleanField(default=False)
    instructor=models.ForeignKey(User,null=True,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.pk)
    def save(self):
        super().save() 
        img=Image.open(self.image.path)
        output_size = (1200,1200)
        img.thumbnail(output_size)
        img.save(self.image.path)
    


class Section(models.Model) : 
    name=models.CharField(max_length=100)
    course=models.ForeignKey(Course,on_delete=models.CASCADE)

class Lecture(models.Model):
    name=models.TextField(max_length=100)
    content=models.FileField(upload_to='lecture')
    course=models.ForeignKey(Course,on_delete=models.CASCADE)
    section=models.ForeignKey(Section,on_delete=models.CASCADE)

class video_content(models.Model):
    video = models.FileField(upload_to= 'video' , blank=True , null=True)
    lecture = models.ForeignKey(Lecture , null = True , on_delete=models.CASCADE)


    





