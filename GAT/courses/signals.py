from django.db.models.signals import post_save
from django.dispatch import receiver
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from .models import Course
from accounts.models import Notification


@receiver(post_save, sender=Notification)
def announce_new_user(sender, instance, created, **kwargs):
    if created:
        channel_layer = get_channel_layer()
        id=str(instance.id)
        content=id+instance.content
        async_to_sync(channel_layer.group_send)('gossip', {'type': 'send_notification','notification': content})
