from django import forms
from django.contrib.auth import models
from django.forms import ModelForm, fields, widgets
from .models import Category,Course, Lecture, Section,video_content

class categoryForm (ModelForm): 
    class Meta :
        model = Category
        fields=['name','slug']


class courseForm (ModelForm): 
    class Meta :
        model = Course
        fields=['name','category','description','level','image','video']

class SectionForm(ModelForm) :
    class Meta : 
        model= Section 
        fields=['name']
    

class LectureForm(ModelForm) :
    class Meta : 
        model= Lecture 
        fields=['name','content']


class VideoForm(ModelForm) :
    class Meta : 
        model= video_content 
        fields=['video']

