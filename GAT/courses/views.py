from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Category,Course, Section
from .forms import categoryForm,courseForm,SectionForm,LectureForm
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required 
from django.contrib.auth.models import User
from accounts.models import Notification
from django.shortcuts import get_object_or_404
import json
from .tasks import *
from celery import shared_task
from .utils import save_image

# Create your views here.
def displayCoursesAdmin(request):
    courses=Course.objects.all()
    return render(request,'courses/admin-course-overview.html',{'courses':courses})



def approveCourse(request,courseId): 
    course=Course.objects.get(id=courseId)
    course.is_approved=True
    course.save()
    return redirect('displayCoursesAdmin')


def addCourse(request):
    form=courseForm(request.POST or None, request.FILES or None)
    if request.is_ajax() and request.method=='POST':
        if form.is_valid(): 
            course=form.save(commit=False)
            user=User.objects.filter(username=request.user).first()
            course.instructor=user
            content="A New course '"+course.name+"' has been added by "+str(request.user)
            typee="Course Notification" 
            notification=Notification(content=content,user=user,notif_type=typee)
            notification.save()
            course.image=None
            course.save()
            # send the image to be saved by a worker
            save_image(form.cleaned_data['image'], course)
            image=json.dumps(str(course.image))
            res_str = image.replace('"', '') 
            image='pics/'+res_str
            video=json.dumps(str(course.video))
            vid_str=video.replace('"','')
            video='pics/'+vid_str
            form=courseForm()
            return JsonResponse({'message':'Uploaded !'})
    return render(request,'courses/add-course.html',{'form':form})


def displaySectionsCourse(request): 
    sections=Section.objects.all()
    return render(request,'courses/add-course.html',{'sections':sections})


def displayInstructorCourse(request):
    user=User.objects.filter(username=request.user).first()
    courses=user.course_set.all()
    return render(request,'courses/instructor-courses.html',{'courses':courses})

def courseFiltre(request):
    courses=Course.objects.all()
    return render(request,'courses/course-filter-list.html',{'courses':courses})


def courseDetails(request,courseId) : 
    course=Course.objects.get(id=courseId)
    print(course)
    return render(request,'courses/course-single.html',{'course':course})


def courseCategory(request):
    categories=Category.objects.all()
    form=categoryForm()
    return render(request,'courses/admin-course-category.html',{'form':form,'categories':categories})


@login_required(login_url='login')    
def addCategory(request):
    form=categoryForm()
    if request.method == 'POST' : 
        print(request.POST)
        form=categoryForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('courseCategory')
    return render(request,'courses/admin-course-category.html',{'form':form})

@csrf_exempt
def updateCategory(request):
    print(request.POST)
    return render(request,'courses/admin-course-category.html') 

@csrf_exempt
def addSection(request) : 
    form=SectionForm()
    if request.is_ajax() and request.method== 'POST' : 
        form=SectionForm(request.POST)
        course=Course.objects.last()
        section=form.save(commit=False) 
        section.course=course 
        section.save()
    return  JsonResponse({'message':'Uploaded !'})



@csrf_exempt
def addLecture(request) : 
    form=LectureForm()
    if request.is_ajax() and request.method== 'POST' : 
        form=LectureForm(request.POST)
        course=Course.objects.last()
        section=Section.objects.last()      
        lecture=form.save(commit=False) 
        lecture.course=course 
        lecture.section=section
        lecture.save()
    else : 
        print(form.errors)
    return  JsonResponse({'message':'Uploaded !'})
