from django.urls import path
from . import views 
urlpatterns = [
    path('displayCoursesAdmin',views.displayCoursesAdmin,name='displayCoursesAdmin'),
    path('add-course',views.addCourse,name='addCourse'),
    path('diplay-Instructor-course',views.displayInstructorCourse,name='displayInstructorCourse'),
    path('course-filtre',views.courseFiltre,name='courseFiltre'),
    path('admin-course-category',views.courseCategory,name='courseCategory'),
    path('admin-add-category',views.addCategory,name='addCategory'),
    path('add-section',views.addSection,name='addSection'),
    path('add-lecture',views.addLecture,name='addLecture'),
    path('display-section',views.displaySectionsCourse,name='displaySection'),
    path('admin-update-category',views.updateCategory,name='updateCategory'),
    path('course-details/<int:courseId>/',views.courseDetails,name='course-details'),
    path('approveCourse/<int:courseId>/',views.approveCourse,name='approveCourse')
]
