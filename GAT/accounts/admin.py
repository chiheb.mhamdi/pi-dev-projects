from django.contrib import admin
from .models import RequestInstructor,Notification
# Register your models here.
admin.site.register(RequestInstructor)
admin.site.register(Notification)
