from django.conf.urls import url
from django.urls import path
from . import views 
urlpatterns = [
    path('register',views.register,name='register'),
    path('login',views.loginUser,name='login'),
    path('logout',views.logoutUser,name='logout'),
    path('display-instructors',views.displayInstructors,name='displayInstructors'),
    path('display-students',views.displayStudents,name='displayStudents'),
    path('dashboard-admin',views.dashboardAdmin,name='dashboardAdmin'),
    path('dashboard-students',views.dashboardStudent,name='dashboard-students'),
    path('dashboard-instructors',views.dashboardInstructor,name='dashboardInstructor'),
    path('edit-profile',views.editProfile,name='edit-profile'),
    path('beInstructor',views.beInstructor,name='beInstructor'),
    path('displayRequestsInstructor',views.displayRequestsInstructor,name='displayRequestsInstructor'),
    path('acceptRequest/<int:requestId>/',views.acceptRequest,name='acceptRequest'),
    path('checkNotification/<int:notifId>/',views.updateNotificationStatus,name='checkNotification')

]