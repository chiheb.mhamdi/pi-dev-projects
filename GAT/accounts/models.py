from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class RequestInstructor(models.Model):
     first_name=models.CharField(max_length=100)
     last_name=models.CharField(max_length=100)
     email=models.EmailField(max_length=254)
     description=models.CharField(max_length=254)
     user=models.OneToOneField(User,on_delete=models.CASCADE)


class Notification(models.Model):
    content=models.CharField(max_length=100)
    user=models.ForeignKey(User,null=True,on_delete=models.CASCADE)
    created_date = models.DateTimeField(default=timezone.now)
    notif_type=models.CharField(max_length=100,default='')
    is_viewed=models.BooleanField(default=False)