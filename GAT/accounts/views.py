from django.http import request
from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User
from django.contrib.auth import authenticate,login,logout
from .forms import CreateUserForm 
from .decorators import unauthenticated_user,allowed_users
from django.contrib.auth.decorators import login_required
from .models import Notification, RequestInstructor 

# Create your views here.
@unauthenticated_user
def register(request):
    form=CreateUserForm()
    if request.method == 'POST' :
        form=CreateUserForm(request.POST )
        if form.is_valid():
            user=form.save()
            group=Group.objects.get(name='student')
            user.groups.add(group)
            print(user)
            return redirect('login')
        else : 
         form=CreateUserForm()
    return render(request,'accounts/sign-up.html',{'form':form})

@unauthenticated_user
def loginUser(request) :
    if request.method == 'POST' :
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(username=username,password=password) 
        if user is not None : 
            login(request,user)
            return redirect("redirect-user")
    return render(request,'accounts/sign-in.html') 

def logoutUser(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def displayInstructors(request): 
    return render(request,'accounts/admin-instructor.html') 



def dashboardAdmin(request):
    return render(request,'accounts/admin-dashboard.html')


def dashboardStudent(request):
    return render(request,'accounts/dashboard-student.html')

def editProfile(request) : 
    return render(request,'accounts/profile-edit.html')


@login_required(login_url='login')
@allowed_users(allowed_roles=['admin'])
def displayStudents(request): 
    return render(request,'accounts/admin-students.html')    

@login_required(login_url='login')
@allowed_users(allowed_roles=['admin','teacher','student'])
def dashboardInstructor(request):
    return render(request,'accounts/dashboard-instructor.html')


def beInstructor(request) :
    if request.method=='POST' :
        first_name=request.POST['firstname']
        last_name=request.POST['lastname'] 
        email=request.POST['email']
        description=request.POST['description'] 
        user=User.objects.filter(username=request.user).first()
        requestIn=RequestInstructor.objects.create(first_name=first_name,last_name=last_name,email=email,description=description,user=user)
        content= " A Request has been sent to be an instructor by "+user.username 
        type="Request Notification"
        notification=Notification(content=content,user=user,notif_type=type) 
        notification.save()
        requestIn.save()
        return redirect('/')
    return render(request,'accounts/beInstructor.html')

def displayRequestsInstructor(request): 
    requests=RequestInstructor.objects.all()
    return render(request,'accounts/requestsInstructor.html',{'requests':requests})

def acceptRequest(request,requestId):
    requestIn=RequestInstructor.objects.filter(id=requestId).first()
    user=User.objects.filter(id=requestIn.user.id).first() 
    group=Group.objects.get(name='teacher')
    user.groups.add(group)
    return render(request,'accounts/requestsInstructor.html')

def updateNotificationStatus(request,notifId) : 
    notif=Notification.objects.get(id=notifId)
    notif.is_viewed=True
    notif.save()
    return redirect('displayCoursesAdmin')

def refuseRequest(request) : 
    pass




